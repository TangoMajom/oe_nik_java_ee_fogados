package com.example.sportsbetting.domain;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class User {

    @Id
    String email;

    String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public static class Builder{
        private User instance;

        public Builder(){
            this.instance = new User();
        }

        public Builder withEmail(String email){
            this.instance.email = email;
            return this;
        }

        public Builder withPassword(String password){
            this.instance.password = password;
            return this;
        }

        public User build(){
            return this.instance;
        }
    }
}
