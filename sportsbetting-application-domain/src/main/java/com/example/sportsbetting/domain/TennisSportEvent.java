package com.example.sportsbetting.domain;

import javax.persistence.Entity;

@Entity
public class TennisSportEvent extends SportEvent {

    public static class Builder extends SportEvent.Builder<TennisSportEvent>{
        public Builder() {
            this.instance = new TennisSportEvent();
        }
    }
}
