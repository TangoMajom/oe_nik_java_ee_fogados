package com.example.sportsbetting.domain;


import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table
public class OutcomeOdd {

    @Id
    @GeneratedValue
    private int id;

    private BigDecimal value;

    private LocalDateTime validFrom;

    private LocalDateTime validUntil;

    @ManyToOne(fetch = FetchType.EAGER)
    private Outcome outcome;

    private Currency currency;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDateTime getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(LocalDateTime validUntil) {
        this.validUntil = validUntil;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "OutcomeOdd{" +
                "id=" + id +
                ", value=" + value +
                ", validFrom=" + validFrom +
                ", validUntil=" + validUntil +
                ", outcome=" + outcome +
                ", currency=" + currency +
                '}';
    }

    public static class Builder {
        private OutcomeOdd instance;

        public Builder() {
            this.instance = new OutcomeOdd();
        }

        public Builder withId(int id) {
            this.instance.id = id;
            return this;
        }


        public Builder withValue(BigDecimal value) {
            this.instance.value = value;
            return this;
        }

        public Builder withValidFrom(LocalDateTime validFrom) {
            this.instance.validFrom = validFrom;
            return this;
        }

        public Builder withValidUntil(LocalDateTime validUntil) {
            this.instance.validUntil = validUntil;
            return this;
        }

        public Builder withOutcome(Outcome outcome) {
            this.instance.outcome = outcome;
            return this;
        }

        public Builder withCurrency(Currency currency) {
            this.instance.currency = currency;
            return this;
        }

        public OutcomeOdd build() {
            return instance;
        }
    }
}
