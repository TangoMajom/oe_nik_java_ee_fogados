package com.example.sportsbetting.domain;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Outcome {

    @Id
    @GeneratedValue
    private int id;

    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    private Bet bet;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "outcome", fetch = FetchType.LAZY)
    private List<OutcomeOdd> outcomeOdds;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private Outcome() {
        this.outcomeOdds = new ArrayList<>();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Bet getBet() {
        return bet;
    }

    public void setBet(Bet bet) {
        this.bet = bet;
    }

    public List<OutcomeOdd> getOutcomeOdds() {
        return outcomeOdds;
    }

    public void addOutcomeOdd(OutcomeOdd outcomeOdd) {
        this.outcomeOdds.add(outcomeOdd);
    }

    public void removeOutcomeOdd(OutcomeOdd outcomeOdd) {
        outcomeOdds.remove(outcomeOdd);
    }

    @Override
    public String toString() {
        return "Outcome{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", bet=" + bet +
                ", outcomeOdds=" + outcomeOdds +
                '}';
    }

    public static class Builder {
        private Outcome instance;

        public Builder() {
            this.instance = new Outcome();
        }

        public Builder withId(int id) {
            this.instance.id = id;
            return this;
        }


        public Builder withDescription(String description) {
            this.instance.description = description;
            return this;
        }

        public Builder withBet(Bet bet) {
            this.instance.bet = bet;
            return this;
        }

        public Builder withOdd(OutcomeOdd outcomeOdd) {
            this.instance.addOutcomeOdd(outcomeOdd);
            return this;
        }

        public Outcome build() {
            return this.instance;
        }
    }
}
