package com.example.sportsbetting.domain;

public enum BetType {
    WINNER,
    GOALS,
    PLAYER_SCORE,
    NUMBER_OF_SETS,
}
