package com.example.sportsbetting.domain;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Result {

    @Id
    @GeneratedValue
    private int id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Outcome> winnerOutcomes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    private Result(){
        this.winnerOutcomes = new ArrayList<>();
    }

    public List<Outcome> getWinnerOutcomes() {
        return winnerOutcomes;
    }

    public void addOutcome(Outcome outcome){
        this.winnerOutcomes.add(outcome);
    }

    public void removeOutcome(Outcome outcome){
        this.winnerOutcomes.remove(outcome);
    }

    @Override
    public String toString() {
        return "Result{" +
                "id=" + id +
                ", winnerOutcomes=" + winnerOutcomes +
                '}';
    }

    public static class Builder{
        private Result instance;

        public Builder(){
            this.instance = new Result();
        }

        public Builder withId(int id){
            this.instance.id = id;
            return this;
        }


        public Builder withOutcome(Outcome outcome){
            this.instance.addOutcome(outcome);
            return this;
        }

        public Result build(){
            return this.instance;
        }
    }
}
