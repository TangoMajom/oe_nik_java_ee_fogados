package com.example.sportsbetting.domain;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public abstract class SportEvent {

    @Id
    @GeneratedValue
    private int id;

    private String title;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "event", fetch = FetchType.LAZY)
    private List<Bet> bets;
    @OneToOne(cascade = CascadeType.ALL)
    private Result result;

    SportEvent(){
        this.bets = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void addBet(Bet bet){
        this.bets.add(bet);
    }

    public void removeBet(Bet bet){
        this.bets.remove(bet);
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "SportEvent{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", bets=" + bets +
                ", result=" + result +
                '}';
    }

    public static abstract class Builder<T extends SportEvent>{
        protected T instance;

        public Builder withId(int id){
            this.instance.setId(id);
            return this;
        }

        public Builder withTitle(String title) {
            this.instance.setTitle(title);
            return this;
        }

        public Builder withStartDate(LocalDateTime startDate) {
            this.instance.setStartDate(startDate);
            return this;
        }

        public Builder withEndDate(LocalDateTime endDate) {
            this.instance.setEndDate(endDate);
            return this;
        }

        public Builder addBet(Bet bet){
            this.instance.addBet(bet);
            return this;
        }

        public Builder withResult(Result result) {
            this.instance.setResult(result);
            return this;
        }

        public T build(){
            return this.instance;
        }
    }
}
