package com.example.sportsbetting.domain;

import javax.persistence.Entity;

@Entity
public class FootballSportEvent extends SportEvent{

    public static class Builder extends SportEvent.Builder<FootballSportEvent>{
        public Builder() {
            this.instance = new FootballSportEvent();
        }
    }
}
