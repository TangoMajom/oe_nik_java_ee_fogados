package com.example.sportsbetting.domain;


import javax.persistence.Entity;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class Player extends User{

    private String name;

    private Integer accountNumber;

    private BigDecimal balance;

    private LocalDateTime birth;

    private Currency currency;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Integer accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public LocalDateTime getBirth() {
        return birth;
    }

    public void setBirth(LocalDateTime birth) {
        this.birth = birth;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", accountNumber=" + accountNumber +
                ", balance=" + balance +
                ", birth=" + birth +
                ", currency=" + currency +
                '}';
    }

    public static class Builder extends User.Builder{
        private Player instance;

        public Builder(){
            this.instance = new Player();
        }

        public Builder withEmail(String email){
            this.instance.email = email;
            return this;
        }

        public Builder withPassword(String password){
            this.instance.password = password;
            return this;
        }

        public Builder withName(String name) {
            this.instance.name = name;
            return this;
        }

        public Builder withAccountNumber(Integer accountNumber) {
            this.instance.accountNumber = accountNumber;
            return this;
        }

        public Builder withBalance(BigDecimal balance) {
            this.instance.balance = balance;
            return this;
        }

        public Builder withBirth(LocalDateTime birth) {
            this.instance.birth = birth;
            return this;
        }

        public Builder withCurrency(Currency currency) {
            this.instance.currency = currency;
            return this;
        }

        public Player build(){
            return this.instance;
        }
    }
}
