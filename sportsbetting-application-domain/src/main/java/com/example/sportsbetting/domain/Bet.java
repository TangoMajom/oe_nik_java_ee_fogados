package com.example.sportsbetting.domain;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Bet")
public class Bet {

    @Id
    @GeneratedValue
    private int id;

    private String description;

    private BetType type;

    @ManyToOne(fetch = FetchType.EAGER)
    private SportEvent event;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bet", fetch = FetchType.LAZY)
    private List<Outcome> outcomes;

    private Bet() {
        this.outcomes = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BetType getType() {
        return type;
    }

    public void setType(BetType type) {
        this.type = type;
    }

    public SportEvent getEvent() {
        return event;
    }

    public void setEvent(SportEvent event) {
        this.event = event;
    }

    public List<Outcome> getOutcomes() {
        return outcomes;
    }

    public void addOutcome(Outcome outcome) {
        this.outcomes.add(outcome);
    }

    public void removeOutcome(Outcome outcome) {
        this.outcomes.remove(outcome);
    }

    @Override
    public String toString() {
        return "Bet{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", type=" + type +
                ", event=" + event +
                ", outcomes=" + outcomes +
                '}';
    }

    public static class Builder {
        private Bet instance;

        public Builder() {
            this.instance = new Bet();
        }

        public Builder withDescription(String description) {
            this.instance.description = description;
            return this;
        }

        public Builder withId(int id) {
            this.instance.id = id;
            return this;
        }

        public Builder withType(BetType type) {
            this.instance.type = type;
            return this;
        }

        public Builder withEvent(SportEvent event) {
            this.instance.event = event;
            return this;
        }

        public Builder withOutcome(Outcome outcome) {
            this.instance.addOutcome(outcome);
            return this;
        }

        public Bet build() {
            return this.instance;
        }
    }
}
