package com.example.sportsbetting.domain;



import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table
public class Wager {

    @Id
    @GeneratedValue
    private int id;

    private BigDecimal amount;
    private LocalDateTime timestampCreated;
    private boolean processed;
    private boolean win;
    @OneToOne(cascade = CascadeType.ALL)
    private OutcomeOdd odd;
    @OneToOne(cascade = CascadeType.ALL)
    private Player player;

    private Currency currency;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getTimestampCreated() {
        return timestampCreated;
    }

    public void setTimestampCreated(LocalDateTime timestampCreated) {
        this.timestampCreated = timestampCreated;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public OutcomeOdd getOdd() {
        return odd;
    }

    public void setOdd(OutcomeOdd odd) {
        this.odd = odd;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Wager{" +
                "id=" + id +
                ", amount=" + amount +
                ", timestampCreated=" + timestampCreated +
                ", processed=" + processed +
                ", win=" + win +
                ", odd=" + odd +
                ", player=" + player +
                ", currency=" + currency +
                '}';
    }

    public static class Builder{
        private Wager instance;

        public Builder(){
            this.instance = new Wager();
        }

        public Builder withId(int id){
            this.instance.id = id;
            return this;
        }


        public Builder withAmount(BigDecimal amount) {
            this.instance.amount = amount;
            return this;
        }

        public Builder withTimestampCreated(LocalDateTime timestampCreated) {
            this.instance.timestampCreated = timestampCreated;
            return this;
        }

        public Builder withProcessed(boolean processed) {
            this.instance.processed = processed;
            return this;
        }

        public Builder withWin(boolean win) {
            this.instance.win = win;
            return this;
        }

        public Builder withOdd(OutcomeOdd odd) {
            this.instance.odd = odd;
            return this;
        }

        public Builder withPlayer(Player player) {
            this.instance.player = player;
            return this;
        }

        public Builder withCurrency(Currency currency) {
            this.instance.currency = currency;
            return this;
        }

        public Wager build(){
            return this.instance;
        }
    }
}
