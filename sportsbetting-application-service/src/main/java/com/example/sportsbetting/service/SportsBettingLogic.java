package com.example.sportsbetting.service;


import com.example.sportsbetting.domain.*;
import com.example.sportsbetting.repository.PlayerRepository;
import com.example.sportsbetting.repository.ResultRepository;
import com.example.sportsbetting.repository.SportEventRepository;
import com.example.sportsbetting.repository.WagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@Transactional
public class SportsBettingLogic implements SportsBettingService {

    private Player player;
    private List<SportEvent> events = new ArrayList<>();
    private List<Wager> wagers = new ArrayList<>();
    private static Random random = new Random();

    @Autowired
    private SportEventRepository eventRepository;
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private ResultRepository resultRepository;
    @Autowired
    private WagerRepository wagerRepository;

    public SportsBettingLogic(){}


    @Override
    public void savePlayer(Player player) {
        this.player = player;
        this.playerRepository.save(player);
    }

    @Override
    public Player findPlayer() {
        Iterable<Player> players = this.playerRepository.findAll();
        return players.iterator().hasNext() ? players.iterator().next() : null;
    }

    @Override
    public List<SportEvent> findAllSportEvents() {
        this.events = new ArrayList<>();

        Iterable<SportEvent> events = eventRepository.findAll();
        events.forEach(this.events::add);

        return this.events;
    }

    @Override
    public void saveWager(Wager wager) {
        this.wagers.add(wager);
        this.player.setBalance(this.player.getBalance().subtract(wager.getAmount()));
        this.wagerRepository.save(wager);
        this.playerRepository.save(player);
    }

    @Override
    public List<Wager> findAllWager() {
        this.wagers.clear();

        Iterable<Wager> events = wagerRepository.findAll();
        events.forEach(this.wagers::add);

        return this.wagers;
    }

    @Override
    public void calculateResults() {
        //Simulating sport events
        for (SportEvent event : this.events) {
            event.setResult(new Result.Builder().build());

            for (Bet bet : event.getBets()) {
                event.getResult().addOutcome(this.chooseRandomOutcome(bet));
            }
        }

        //Comparing event results with player wagers
        for (Wager wager : this.wagers) {
            SportEvent event = wager.getOdd().getOutcome().getBet().getEvent();

            boolean isWagerWon = event.getResult().getWinnerOutcomes().stream()
                    .anyMatch(outcome -> outcome == wager.getOdd().getOutcome());

            //Adjusting player balance according to result
            BigDecimal prize;
            if (isWagerWon) {
                wager.setWin(true);
                prize = wager.getAmount().multiply(wager.getOdd().getValue());
            } else {
                wager.setWin(false);
                prize = wager.getAmount().multiply(new BigDecimal(-1));
            }

            wager.getPlayer().setBalance(wager.getPlayer().getBalance().add(prize));
        }

        this.eventRepository.saveAll(this.events);
        this.wagerRepository.saveAll(this.wagers);
        this.playerRepository.save(this.player);
    }

    private Outcome chooseRandomOutcome(Bet bet) {
        return bet.getOutcomes().get(random.nextInt(bet.getOutcomes().size()));
    }
}
