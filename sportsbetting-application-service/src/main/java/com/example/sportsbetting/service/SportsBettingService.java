package com.example.sportsbetting.service;

import com.example.sportsbetting.domain.Player;
import com.example.sportsbetting.domain.SportEvent;
import com.example.sportsbetting.domain.Wager;

import java.util.List;

public interface SportsBettingService {
    void savePlayer(Player player);

    Player findPlayer();

    List<SportEvent> findAllSportEvents();

    void saveWager(Wager wager);

    List<Wager> findAllWager();

    void calculateResults();
}
