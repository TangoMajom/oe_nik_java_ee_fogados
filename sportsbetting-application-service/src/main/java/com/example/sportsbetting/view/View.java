package com.example.sportsbetting.view;



import com.example.sportsbetting.domain.OutcomeOdd;
import com.example.sportsbetting.domain.Player;
import com.example.sportsbetting.domain.SportEvent;
import com.example.sportsbetting.domain.Wager;

import java.math.BigDecimal;
import java.util.List;

public interface View {
    Player readPlayerData();

    void printWelcomeMesssage(Player player);

    void printBalance(Player player);

    void printOutcomeOdds(List<SportEvent> events);

    OutcomeOdd selectOutcomeOdd(List<SportEvent> events);

    BigDecimal readWagerAmount();

    void printWagerSaved(Wager wager);

    void printNotEnoughBalance(Player player);

    void printResult(Player player, List<Wager> wagers);
}
