package com.example.sportsbetting.view;


import com.example.sportsbetting.domain.*;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Transactional
public class ConsoleView implements View {

    private List<Outcome> outcomes;

    @Override
    public Player readPlayerData() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("What is your name?");
            String name = reader.readLine();
            System.out.println("How much money do you have (more than 0)?");
            BigDecimal money = new BigDecimal(reader.readLine());
            System.out.println("What is your currency? (HUF, EUR or USD)");
            Currency currency = Currency.valueOf(reader.readLine());

            return new Player.Builder()
                    .withEmail("test@example.com")
                    .withPassword("karalabe")
                    .withName(name)
                    .withBalance(money)
                    .withCurrency(currency)
                    .withAccountNumber(1111111)
                    .withBirth(LocalDateTime.of(1992,4,23,0,0,0))
                    .build();

        } catch (IOException | NumberFormatException e) {
            return null;
        }
    }

    @Override
    public void printWelcomeMesssage(Player player) {
        System.out.println("Welcome" + player.getName() + "!");
    }

    @Override
    public void printBalance(Player player) {
        System.out.println("Your balance is " + player.getBalance() + " " + player.getCurrency());
    }

    @Override
    public void printOutcomeOdds(List<SportEvent> events) {
        System.out.println("What do you want to bet on? (choose a number or press q for quit)");
        this.outcomes = events
                .stream()
                .flatMap(event -> event.getBets().stream())
                .flatMap(bet -> bet.getOutcomes().stream())
                .collect(Collectors.toList());

        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        for (Outcome outcome : this.outcomes) {
            OutcomeOdd latestOdd = outcome.getOutcomeOdds()
                    .stream()
                    .max(Comparator.comparing(OutcomeOdd::getValidFrom))
                    .get();

            System.out.println((this.outcomes.indexOf(outcome) + 1) + ": " +
                    "Sport event: " + outcome.getBet().getEvent().getTitle() + " " +
                    "(start: " + outcome.getBet().getEvent().getStartDate().format(df) + "), " +
                    "Bet: " + outcome.getBet().getDescription() + ", " +
                    "Outcome: " + outcome.getDescription() + ", " +
                    "Actual odd: " + latestOdd.getValue() + ", " +
                    "Valid between: " + latestOdd.getValidFrom().format(df) + " and " + latestOdd.getValidUntil().format(df) + "."
            );
        }
    }

    @Override
    public OutcomeOdd selectOutcomeOdd(List<SportEvent> events) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int userInput = Integer.parseInt(reader.readLine());
            Outcome selectedOutcome = this.outcomes.get(userInput - 1);
            return selectedOutcome.getOutcomeOdds().get(selectedOutcome.getOutcomeOdds().size() - 1);
        } catch (IOException | NumberFormatException e) {
            return null;
        }
    }

    @Override
    public BigDecimal readWagerAmount() {
        try {
            System.out.println("What amount do you wish to bet on it?");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            return new BigDecimal(reader.readLine());
        } catch (IOException e) {
            return new BigDecimal(0);
        }
    }

    @Override
    public void printWagerSaved(Wager wager) {
        OutcomeOdd odd = wager.getOdd();
        Outcome outcome = odd.getOutcome();
        Bet bet = outcome.getBet();
        SportEvent event = bet.getEvent();

        System.out.println("Wager '" + bet.getDescription() + " = " + outcome.getDescription() + "' of " + event.getTitle() + "[odd: " + odd.getValue() + ", amount: " + wager.getAmount() + "] saved!");
    }

    @Override
    public void printNotEnoughBalance(Player player) {
        System.out.println("You don't have enough money, your balance is " + player.getBalance() + " " + player.getCurrency());
    }

    @Override
    public void printResult(Player player, List<Wager> wagers) {
        System.out.println("Results:");

        for (Wager wager : wagers) {
            Outcome outcome = wager.getOdd().getOutcome();
            Bet bet = outcome.getBet();
            SportEvent event = bet.getEvent();
            System.out.println("Wager '" + bet.getDescription() + " = " + outcome.getDescription() + "' " +
                    "of " + event.getTitle() + " " +
                    "[odd:" + wager.getOdd().getValue() + ", amount: " + wager.getAmount() + "], " +
                    "win: " + wager.isWin());
        }

        this.printBalance(player);
    }
}
