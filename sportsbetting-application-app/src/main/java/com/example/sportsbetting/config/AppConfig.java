package com.example.sportsbetting.config;

import com.example.sportsbetting.App;
import com.example.sportsbetting.service.SportsBettingLogic;
import com.example.sportsbetting.service.SportsBettingService;
import com.example.sportsbetting.view.ConsoleView;
import com.example.sportsbetting.view.View;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;


@Configuration
public class AppConfig {

    @Bean
    public App app(){
        App app = new App(sportsBettingService(),view());
        return app;
    }

    @Bean
    public SportsBettingService sportsBettingService(){
        return new SportsBettingLogic();
    }

    @Bean
    public View view(){
        return new ConsoleView();
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {

        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames("messages/strings");
        source.setDefaultEncoding("UTF-8");
        source.setUseCodeAsDefaultMessage(true);
        source.setFallbackToSystemLocale(true);

        return source;
    }
}
