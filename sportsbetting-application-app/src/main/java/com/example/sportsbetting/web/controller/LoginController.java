package com.example.sportsbetting.web.controller;

import com.example.sportsbetting.domain.Player;
import com.example.sportsbetting.domain.User;
import com.example.sportsbetting.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
@Transactional
public class LoginController {


    @Autowired
    PlayerRepository playerRepository;


    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String loginView(HttpSession session){
        if(session.getAttribute("user") != null){
            return "redirect:/home";
        }
        return "login";
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String password,
                        HttpSession session,
                        ModelMap modelMap){


        Optional<Player> userLookup = this.playerRepository.findById(username);

        User user;

        if(userLookup.isPresent()){
            user = userLookup.get();
        }else {
            modelMap.put("error", "User not registered");
            return "redirect:/login";
        }

        if(username.equalsIgnoreCase(user.getEmail()) && password.equalsIgnoreCase(user.getPassword())) {
            session.setAttribute("user", user);
            return "redirect:/home";
        } else {
            modelMap.put("error", "Invalid credentials");
            return "redirect:/login";
        }
    }

    @RequestMapping(path = "/logout", method = RequestMethod.POST)
    public String logout(HttpSession session){
        session.removeAttribute("user");
        return "redirect:/login";
    }
}
