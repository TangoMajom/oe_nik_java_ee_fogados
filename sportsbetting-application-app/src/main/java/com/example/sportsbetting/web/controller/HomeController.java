package com.example.sportsbetting.web.controller;

import com.example.sportsbetting.AppTestData;
import com.example.sportsbetting.domain.Currency;
import com.example.sportsbetting.domain.Player;
import com.example.sportsbetting.domain.Wager;
import com.example.sportsbetting.repository.PlayerRepository;
import com.example.sportsbetting.repository.SportEventRepository;
import com.example.sportsbetting.repository.WagerRepository;
import com.example.sportsbetting.service.SportsBettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
@Transactional
public class HomeController {

    @Autowired
    private SportsBettingService bettingService;
    @Autowired
    private SportEventRepository eventRepository;
    @Autowired
    private WagerRepository wagerRepository;
    @Autowired
    private PlayerRepository playerRepository;


    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String homepage(HttpSession session, ModelMap modelMap) {

        Player user = (Player) session.getAttribute("user");
        if (user == null) {
            return "redirect:/login";
        }

        modelMap.addAttribute("user", user);
        modelMap.addAttribute("today", LocalDateTime.now());
        modelMap.addAttribute("wagers", this.bettingService.findAllWager());

        return "home";
    }

    @RequestMapping(value = {"/player"}, method = RequestMethod.POST)
    public String updatePlayer(
            @RequestParam("name") String name,
            @RequestParam("birth") String birth,
            @RequestParam("account") Integer accountNumber,
            @RequestParam("currency") String currency,
            @RequestParam("balance") BigDecimal balance,
            HttpSession session) {

        Player user = (Player) session.getAttribute("user");
        if (user == null) {
            return "redirect:/login";
        }

        user.setName(name);
        user.setBirth(LocalDateTime.parse(birth));
        user.setCurrency(Currency.valueOf(currency));
        user.setAccountNumber(accountNumber);
        user.setBalance(balance);

        this.bettingService.savePlayer(user);

        return "redirect:/home";
    }

    @RequestMapping(value= {"/wager/delete"}, method = RequestMethod.POST)
    public String deleteWager(@RequestParam("wager") Integer wagerId, HttpSession session) {
        Player user = (Player) session.getAttribute("user");
        if (user == null) {
            return "redirect:/login";
        }

        List<Wager> wagers = this.bettingService.findAllWager();
        Optional<Wager> wagerLookup = wagers.stream()
                .filter(wager -> wager.getId() == wagerId)
                .findFirst();


        if(wagerLookup.isPresent()){
            Wager wagerToDelete = wagerLookup.get();
            if (wagerToDelete.getOdd().getOutcome().getBet().getEvent().getEndDate().compareTo(LocalDateTime.now()) == 1) {
                this.wagerRepository.deleteById(wagerId);
            }
        }

        return "redirect:/home";
    }

    @RequestMapping(value = {"/init"}, method = RequestMethod.GET)
    public String initApplication() {

        this.playerRepository.deleteAll();

        Player defaultPlayer = new Player.Builder()
                .withEmail("test@example.com")
                .withPassword("karalabe")
                .withName("Tomi")
                .withBalance(new BigDecimal(9000))
                .withCurrency(Currency.HUF)
                .withAccountNumber(1111111)
                .withBirth(LocalDateTime.of(1992,4,23,0,0,0))
                .build();

        this.bettingService.savePlayer(defaultPlayer);

        this.eventRepository.deleteAll();
        this.eventRepository.saveAll(AppTestData.generateTestEvents());


        return "redirect:/home";
    }
}
