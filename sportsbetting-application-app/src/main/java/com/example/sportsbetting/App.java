package com.example.sportsbetting;


import com.example.sportsbetting.domain.OutcomeOdd;
import com.example.sportsbetting.domain.Player;
import com.example.sportsbetting.domain.SportEvent;
import com.example.sportsbetting.domain.Wager;
import com.example.sportsbetting.service.SportsBettingService;
import com.example.sportsbetting.view.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Transactional
public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    @Autowired
    private MessageSource messageSource;

    private Player player;

    @Autowired
    private SportsBettingService service;

    @Autowired
    private View view;


    public App(SportsBettingService service, View view) {
        this.service = service;
        this.view = view;
    }

    private void createPlayer() {
        Player player = this.view.readPlayerData();
        this.service.savePlayer(player);
    }

    private void doBetting() {
        this.view.printBalance(this.player);

        List<SportEvent> events = this.service.findAllSportEvents();
        this.view.printOutcomeOdds(events);

        OutcomeOdd outcomeOdd = this.view.selectOutcomeOdd(events);

        if(outcomeOdd != null){
            BigDecimal wagerAmount = this.view.readWagerAmount();

            if(this.player.getBalance().compareTo(wagerAmount) < 0){
                this.view.printNotEnoughBalance(this.player);
                this.doBetting();
            }

            Wager wager = new Wager.Builder()
                    .withPlayer(this.player)
                    .withAmount(wagerAmount)
                    .withCurrency(this.player.getCurrency())
                    .withOdd(outcomeOdd)
                    .withTimestampCreated(LocalDateTime.now())
                    .withProcessed(false)
                    .withWin(false)
                    .build();

            this.service.saveWager(wager);
            this.view.printWagerSaved(wager);

            this.doBetting();
        }
    }

    private void calculateResults() {
        this.service.calculateResults();
    }

    private void printResults() {
        this.view.printResult(this.player,this.service.findAllWager());
    }

    public void play() {
        //logger.debug(messageSource.getMessage("test",null, Locale.getDefault()));
        this.createPlayer();
        this.player = this.service.findPlayer();
        this.view.printWelcomeMesssage(this.player);
        this.doBetting();
        this.calculateResults();
        this.printResults();
    }
}
