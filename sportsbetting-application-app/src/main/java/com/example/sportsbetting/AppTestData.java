package com.example.sportsbetting;

import com.example.sportsbetting.domain.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AppTestData {
    public static List<SportEvent> generateTestEvents(){
        List<SportEvent> events = new ArrayList<>();

        SportEvent event = new FootballSportEvent.Builder()
                .withTitle("Arsenal vs Chelsea")
                .withStartDate(LocalDateTime.of(2020,1,1,12,0,0))
                .withEndDate(LocalDateTime.of(2020,1,1,14,0,0))
                .build();


        Bet playerBet = new Bet.Builder()
                .withDescription("player Oliver Giroud score")
                .withType(BetType.PLAYER_SCORE)
                .withEvent(event)
                .build();

        Bet scoreBet = new Bet.Builder()
                .withDescription("number of scored goals")
                .withType(BetType.GOALS)
                .withEvent(event)
                .build();

        Bet winnerBet = new Bet.Builder()
                .withDescription("winner")
                .withType(BetType.WINNER)
                .withEvent(event)
                .build();

        Outcome playerOutcome = new Outcome.Builder()
                .withDescription("1")
                .withBet(playerBet)
                .build();

        Outcome scoreOutcome = new Outcome.Builder()
                .withDescription("3")
                .withBet(scoreBet)
                .build();

        Outcome winnerOutcome1 = new Outcome.Builder()
                .withDescription("Arsenal")
                .withBet(winnerBet)
                .build();

        Outcome winnerOutcome2 = new Outcome.Builder()
                .withDescription("Chelsea")
                .withBet(winnerBet)
                .build();

        OutcomeOdd playerOdd = new OutcomeOdd.Builder()
                .withOutcome(playerOutcome)
                .withValue(new BigDecimal(2))
                .withValidFrom(LocalDateTime.of(2020,1,1,12,0,0))
                .withValidUntil(LocalDateTime.of(2020,1,1,14,0,0))
                .build();

        OutcomeOdd scoreOdd = new OutcomeOdd.Builder()
                .withOutcome(scoreOutcome)
                .withValue(new BigDecimal(3))
                .withValidFrom(LocalDateTime.of(2020,1,1,12,0,0))
                .withValidUntil(LocalDateTime.of(2020,1,1,14,0,0))
                .build();

        OutcomeOdd winnerOdd1 = new OutcomeOdd.Builder()
                .withOutcome(winnerOutcome1)
                .withValue(new BigDecimal(2))
                .withValidFrom(LocalDateTime.of(2020,1,1,12,0,0))
                .withValidUntil(LocalDateTime.of(2020,1,1,14,0,0))
                .build();

        OutcomeOdd winnerOdd2 = new OutcomeOdd.Builder()
                .withOutcome(winnerOutcome2)
                .withValue(new BigDecimal(3))
                .withValidFrom(LocalDateTime.of(2020,1,1,12,0,0))
                .withValidUntil(LocalDateTime.of(2020,1,1,14,0,0))
                .build();

        playerOutcome.addOutcomeOdd(playerOdd);
        scoreOutcome.addOutcomeOdd(scoreOdd);
        winnerOutcome1.addOutcomeOdd(winnerOdd1);
        winnerOutcome2.addOutcomeOdd(winnerOdd2);

        playerBet.addOutcome(playerOutcome);
        scoreBet.addOutcome(scoreOutcome);
        winnerBet.addOutcome(winnerOutcome1);
        winnerBet.addOutcome(winnerOutcome2);

        event.addBet(playerBet);
        event.addBet(scoreBet);
        event.addBet(winnerBet);
        events.add(event);

        return events;
    }
}
