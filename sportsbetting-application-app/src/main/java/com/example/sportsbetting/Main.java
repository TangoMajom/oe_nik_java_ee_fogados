package com.example.sportsbetting;

import com.example.sportsbetting.config.AppConfig;
import com.example.sportsbetting.config.RepositoryConfig;
import com.example.sportsbetting.repository.SportEventRepository;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        try (ConfigurableApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class, RepositoryConfig.class)) {
            SportEventRepository eventRepository = appContext.getBean(SportEventRepository.class);
            eventRepository.saveAll(AppTestData.generateTestEvents());

            App app = appContext.getBean(App.class);
            app.play();
        }
    }
}
