<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<div class="card blue-card ">
    <div class="card-header blue-card-header">Account details</div>
    <div class="card-body ">
        <form method="post" action="player">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Name</span>
                </div>
                <input name="name" type="text" class="form-control" aria-label="name" aria-describedby="basic-addon1" value="${user.getName()}">
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon2">Date of Birth</span>
                </div>
                <input name="birth" type="text" class="form-control" aria-label="birth" aria-describedby="basic-addon1" value="${user.getBirth()}">
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">Account number</span>
                </div>
                <input name="account" type="text" class="form-control" aria-label="account"
                       aria-describedby="basic-addon1" value="${user.getAccountNumber()}">
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon4">Currency</span>
                </div>
                <select name="currency" class="custom-select" id="inputGroupSelect01" value="${user.getCurrency().toString()}">
                    <option value="HUF">HUF</option>
                    <option value="EUR">EUR</option>
                    <option value="USD">USD</option>
                </select>
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon5">Balance</span>
                </div>
                <input name="balance" type="text" class="form-control" aria-label="balance"
                       aria-describedby="basic-addon1" value="${user.getBalance()}">
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>

<div class="card blue-card">
    <div class="card-header blue-card-header">Wagers</div>
    <div class="card-body ">
        <table class="table">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">#</th>
                <th scope="col">Event title</th>
                <th scope="col">Event type</th>
                <th scope="col">Bet type</th>
                <th scope="col">Outcome value</th>
                <th scope="col">Outcome odd</th>
                <th scope="col">Wager amount</th>
                <th scope="col">Winner</th>
                <th scope="col">Processed</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="wager" items="${wagers}">
            <tr>
                <th>
                    <c:if test="${wager.getOdd().getOutcome().getBet().getEvent().getEndDate().compareTo(today) == 1}">
                        <form method="post" action="wager/delete">
                            <input name="wager" type="hidden" value="${wager.getId()}">
                            <button type="submit" class="btn btn-primary">Remove</button>
                        </form>
                    </c:if>
                </th>
                <th scope="row">${wager.id}</th>
                <td>${wager.getOdd().getOutcome().getBet().getEvent().getTitle()}</td>
                <td>${wager.getOdd().getOutcome().getBet().getEvent().getClass().getSimpleName()}</td>
                <td>${wager.getOdd().getOutcome().getBet().getType().toString()}</td>
                <td>${wager.getOdd().getOutcome().getDescription()}</td>
                <td>${wager.getOdd().getValue()}</td>
                <td>${wager.getAmount()} ${user.getCurrency().toString()}</td>
                <td>${wager.isWin() ? "Yes" : "No"}</td>
                <td>${wager.isProcessed() ? "Yes" : "No"}</td>
            </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>