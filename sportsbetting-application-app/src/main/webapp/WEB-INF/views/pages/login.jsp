<%--
  Created by IntelliJ IDEA.
  User: OargaTamas
  Date: 2019. 11. 29.
  Time: 19:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container-fluid login-header text-center align-content-center justify-content-center align-middle">
    <h1 class="text-white">Welcome to SportsBet!</h1>
    <h5 class="text-white">Sports betting is the activity of predicting sports results and placing a wager on the outcome.</h5>
</div>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm"></div>
        <div class="col-sm">
            <h3 class="text-center"><span class="text-blue">Login</span> or <span class="text-blue">Register</span> to start!</h3>
            <div class="card login-card blue-card">
                <div class="card-header blue-card-header">
                    <h4 class="text-white">Login</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="login">
                        <div class="form-group">
                            <label for="username">Email</label>
                            <input type="email" class="form-control" name="username" id="username"/>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm"></div>
        <div class="col-sm"></div>
    </div>

</div>

